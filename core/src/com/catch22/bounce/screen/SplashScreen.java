package com.catch22.bounce.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.catch22.bounce.game.Bounce;

import static com.catch22.bounce.game.Bounce.master;
import static com.catch22.bounce.game.ResourceManager.loadAudio;
import static com.catch22.bounce.game.ResourceManager.loadFileHandle;
import static com.catch22.bounce.game.ResourceManager.loadMaps;
import static com.catch22.bounce.game.ResourceManager.loadRenderers;

public class SplashScreen implements Screen
{
    private static Texture catch22Logo = new Texture("catch22.png");
    private static SpriteBatch spriteBatch = new SpriteBatch();

    private static final int SPLASH_SCREEN_TIMEOUT = 5; //seconds

    private static Thread loader = new Thread()
    {
        @Override
        public void run()
        {
            try
            {
                sleep(SPLASH_SCREEN_TIMEOUT * 1000);
            } catch(InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void show()
    {
        loader.start();

        loadMaps();
        loadRenderers();
        loadFileHandle();
        loadAudio();
    }

    @Override
    public void render(float delta)
    {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glClearColor(1, 1, 1, 1);

        spriteBatch.begin();
        spriteBatch.draw(catch22Logo, 0, 0);
        spriteBatch.end();

        if (Gdx.input.isTouched())
        {
            master.setScreen(Bounce.game = new Game());
        }
    }

    @Override
    public void resize(int width, int height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {

    }
}
