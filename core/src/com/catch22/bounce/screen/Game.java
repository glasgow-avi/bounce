package com.catch22.bounce.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.catch22.bounce.audio.Audio;
import com.catch22.bounce.audio.MusicPlayer;
import com.catch22.bounce.game.Bounce;
import com.catch22.bounce.game.ResourceManager;
import com.catch22.bounce.gui.Button;
import com.catch22.bounce.gui.Event;
import com.catch22.bounce.level.Level;
import com.catch22.bounce.sprites.Ball;
import com.catch22.bounce.sprites.Enemy;
import com.catch22.bounce.sprites.ExtraLife;
import com.catch22.bounce.sprites.Message;
import com.catch22.bounce.sprites.WarpZone;

import static com.catch22.bounce.game.Bounce.levels;

public class Game implements Screen
{
    //world creation data
    private static Stage stage = new Stage();
    public static Ball player; //the ball
    public static SpriteBatch batch; //the sprite batch we use to draw the sprites
    public static float frameRate = 1 / 70f; //frames per second
    public static int velocityIterations = 8, positionIterations = 3; //velocity iterations, position iterations

    //level data
    public static int activeLevel = 1; //level the user is currently at

    //map data
    public static TiledMap activeMap; //map of the level the user is currently at

    //renderers
    public static OrthographicCamera camera; //camera to follow the balls around the world
    private Viewport gamePort; //the orthographic camera's viewport
    private OrthogonalTiledMapRenderer renderer; //tile renderer

    //debug data
    private Box2DDebugRenderer debugRenderer = new Box2DDebugRenderer(); //box2d debug renderer
    public static int ballTileX, ballTileY; //the level the ball is at
    public static float tileSize; //side of the tile, size

    public static boolean pause;
    public static Texture pauseScreen;

    public static TutorialManager tutorialManager;
    private boolean renderBall; //alternates true and false when the ball is dying to create flashing effect
    public static boolean gameOver;
    public static boolean theBeginning = true;

    public Game()
    {
        activeLevel = 3;
        //screens initialization
        batch = new SpriteBatch();

        Game.pause = false;

        Gdx.input.setCatchBackKey(true);

        renderer = new OrthogonalTiledMapRenderer(levels[activeLevel].map, 1 / Bounce.PPM);
        camera = new OrthographicCamera();
        gamePort = new FitViewport(Bounce.width / Bounce.PPM, Bounce.height / Bounce.PPM, camera);
        camera.position.set(gamePort.getWorldWidth() / 2, gamePort.getWorldHeight() / 2, 0);
        batch = new SpriteBatch();
        player = new Ball(levels[activeLevel], levels[activeLevel].initialPosition.x, levels[activeLevel].initialPosition.y); //initialize the ball in the new world
    }

    @Override
    public void render(float delta)
    {
        //math
        if(!pause)
            update();

        //clear the screen
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.setProjectionMatrix(camera.combined);

        stage.act();

        //draw the player
        batch.begin();
        batch.draw(Bounce.Settings.nightMode ? MainMenu.nightModeBackground : MainMenu.dayBackground, camera.position.x - camera.viewportWidth / 2, camera.position.y - camera.viewportHeight / 2, camera.viewportWidth, camera.viewportHeight);
        if(Ball.ballDestroyer != null && Ball.ballDestroyer.isAlive())
        {
            renderBall = !renderBall;
            if(renderBall)
                player.draw(batch);
        }
        else
        {






































































































































































           player.draw(batch);
        }
        batch.end();

        drawSprites();

//        debugRenderer.render(levels[activeLevel].world, batch.getProjectionMatrix());

        renderer.render(new int[]{1, 2}); //draw the map
         levels[activeLevel].warpZone.render(batch);

        if(Bounce.Settings.nightMode)
        {
            levels[activeLevel].rayHandler.setCombinedMatrix(camera.combined);
            levels[activeLevel].rayHandler.render();
        }

        if(pause)
        {
            Hud.renderPause(pauseScreen);
            pause = !Gdx.input.isTouched() && !Gdx.input.isKeyPressed(Input.Keys.SPACE);
            if(gameOver && !pause)
                Bounce.backPressed = true;
        }

        stage.draw();

        //draw the score counters
        Hud.render();
    }

    @Override
    public void resize(int width, int height) {
        if (!pause)
            gamePort.update(width, height);
    }

    public static void setPause(Texture texture)
    {
        pauseScreen = texture;
        pause = true;
    }

    public void handleInput()
    {
        if(TutorialManager.tutorial || Ball.ballDestroyer != null && Ball.ballDestroyer.isAlive()) //disable input during a tutorial or a ball dying animation
            return;

        //input for desktop platform
        if("Desktop".equals(Bounce.Settings.platform))
        {
            if(Gdx.input.isKeyPressed(Input.Keys.LEFT))
            {
                if(player.body.getLinearVelocity().x >= -Ball.maxVelocity)
                    player.body.applyLinearImpulse(new Vector2(-Ball.sidewaysImpulse, 0), player.body.getWorldCenter(), true);
            }
            if(Gdx.input.isKeyPressed(Input.Keys.RIGHT))
            {
                if(player.body.getLinearVelocity().x <= Ball.maxVelocity)
                    player.body.applyLinearImpulse(new Vector2(Ball.sidewaysImpulse, 0), player.body.getWorldCenter(), true);
            }
            if(Gdx.input.isKeyJustPressed(Input.Keys.SPACE))
                player.jump();

            if(Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) //this shit has been written purely for debugging purposes
            {
                Gdx.app.log("Ball is at", ballTileX + ", " + ballTileY + " in level " + activeLevel);
            }
        }

        //input for android device
        //this works, don't touch it
        if(Gdx.input.getAccelerometerY() < 0 || Gdx.input.isKeyPressed(Input.Keys.LEFT))
            player.move(false, Gdx.input.getAccelerometerY());
        else if(Gdx.input.getAccelerometerY() > 2 || Gdx.input.isKeyPressed(Input.Keys.RIGHT))
            player.move(true, Gdx.input.getAccelerometerY());
        if(Gdx.input.justTouched())
            player.jump();

        if(Gdx.input.isKeyPressed(Input.Keys.BACK))
        {
            Bounce.backPressed = true;
            Gdx.app.exit();
        }
    }

    public void update()
    {
        handleInput();

        //warp to the next level
        if(player.warp)
        {
            activeMap = levels[++activeLevel].map;
            player = new Ball(levels[activeLevel], levels[activeLevel].initialPosition.x, levels[activeLevel].initialPosition.y); //initialize the ball in the new world
            renderer.setMap(levels[activeLevel].map); //set the renderer to the new world
            ResourceManager.save(); //save the game
            player.warp = false; //don't have to warp it in the next update
        }

        levels[activeLevel].world.step(frameRate, velocityIterations, positionIterations); //step world

        ballTileX = (int) (player.body.getPosition().x / tileSize);
        ballTileY = (int) (player.body.getPosition().y / tileSize);

        //update camera
        if(!TutorialManager.tutorial)
        {
            camera.position.x = player.body.getPosition().x; //camera always follows ball on x
            camera.position.y = gamePort.getWorldHeight() / 2 + ((ballTileY - 1) / 7) * 7 * gamePort.getWorldHeight() / 8; //camera only translates along y, when ball is on different levels
        }
        else
        {
            camera.translate(tutorialManager.tutorialX, tutorialManager.tutorialY);
        }
        renderer.setView(camera);
        camera.update();

        //update player
        player.update();

        //update all enemies
        for(Enemy enemy : levels[activeLevel].enemies)
            enemy.update();

        //update light
        if(Bounce.Settings.nightMode)
            levels[activeLevel].rayHandler.update();
    }

    private static void drawSprites()
    {
        //draw all extra lives
        for(ExtraLife extraLife : levels[activeLevel].extraLives)
            extraLife.render(batch);

        //draw all enemies
        for(Enemy enemy : levels[activeLevel].enemies)
            enemy.render(batch);

        //draw all messages
        for(Message message : levels[activeLevel].messages)
            message.render(batch);
    }

    @Override
    public void dispose()
    {
//        //dispose all maps, renderers and players
//        activeMap.dispose();
//        renderer.dispose();
//        try
//        {
//            for(Level l : levels)
//                l.dispose();
//        } catch(NullPointerException notInitializedLevel)
//        {
//
//        }
//
//        for(Array<Enemy> enemiesOfALevel: enemies)
//            for(Enemy enemy : enemiesOfALevel)
//                enemy.dispose();
//
//        debugRenderer.dispose();
//        player.dispose();
    }



    @Override
    public void show()
    {
        Gdx.app.log("Active Level", "" + activeLevel);
        activeMap = levels[activeLevel].map;
        MusicPlayer.play(Bounce.Settings.nightMode ? Bounce.backgroundMusicNight : Bounce.backgroundMusic);
        Gdx.input.setInputProcessor(stage);
        stage.setViewport(gamePort);
    }

    @Override
    public void hide()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void pause()
    {

    }
}