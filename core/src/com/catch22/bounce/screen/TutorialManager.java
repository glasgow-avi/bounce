package com.catch22.bounce.screen;

import com.badlogic.gdx.Gdx;

public class TutorialManager extends Thread
{
    public float tutorialX, tutorialY; //the x and y coordinates of each camera translation
    public static boolean tutorial; //is a tutorial going on or not

    private int ticks;

    public TutorialManager(float tutorialX, float tutorialY, int ticks)
    {
        this.tutorialX = tutorialX;
        this.tutorialY = tutorialY;
        this.ticks = ticks;

        tutorial = true;
    }

    @Override
    public void run()
    {
        int x = ticks / 2;

        if(ticks == 0)
            return;

        while(x --> 0)
        {
            try
            {
                sleep(1000);
            } catch (InterruptedException e)
            {

            }

            Gdx.app.log("Thread", String.valueOf(x));
        }

        tutorialX *= -1;
        tutorialY *= -1;

        //reverse direction

        ticks /= 2;

        while(ticks --> 0)
        {
            try
            {
                sleep(1000);
            } catch (InterruptedException e)
            {

            }
        }

        tutorial = false;
    }
}