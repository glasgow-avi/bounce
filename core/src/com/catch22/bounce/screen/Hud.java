package com.catch22.bounce.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.catch22.bounce.game.Bounce;

public class Hud
{
    private static OrthographicCamera camera; //a separate camera is required because the game is scaled to PPM
    public static BitmapFont font; //the actual font
    private static FreeTypeFontGenerator generator; //font generator
    private static FreeTypeFontGenerator.FreeTypeFontParameter parameter; //properties
    private static final int fontSize = 44; //the font size

    public static int numberOfLivesRemaining = 10; //number of lives remaining

    //constructor
    static
    {
        generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/arcon.otf")); //load the font.otf file
        parameter = new FreeTypeFontGenerator.FreeTypeFontParameter(); //initialize the generator
        parameter.size = fontSize; //set the font size to font size
        parameter.color = new Color(0xf7 / 255f, 0xf7 / 255f, 0xf7 / 255f, 1); //set the colour to marble

        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()); //no scaling to the camera

        font = generator.generateFont(parameter); //generate the font
    }

    //drawing the score text
    public static void render()
    {
        Game.batch.setProjectionMatrix(camera.combined); //set the projection matrix to the HUD's camera
        Game.batch.begin();

        //set the text
        String text = " Level " + (Game.activeLevel) +
                " Checkpoints: " + (Bounce.levels[Game.activeLevel].numberOfCheckpoints - Bounce.levels[Game.activeLevel].numberOfCheckpointsRemaining) + "/" + Bounce.levels[Game.activeLevel].numberOfCheckpoints +
                " Lives: " + (numberOfLivesRemaining == -1 ? 0 : numberOfLivesRemaining);

        //draw the text
        font.draw(Game.batch, text, -camera.viewportWidth / 2, -camera.viewportHeight / 2 + fontSize);

        Game.batch.end();
    }

    public static final Texture beachball = new Texture("pause screens/beachball.png"),
                                glasgowIntroduction = new Texture("pause screens/glasgow Introduction.png"),
                                firstCheckpoint = new Texture("pause screens/first checkpoint.png"),
                                firstWarped = new Texture("pause screens/first warp.png"),
                                gameOver = new Texture("pause screens/game over.png"),
                                gameOverNight = new Texture("pause screens/game over night.png");

    public static void renderPause(Texture texture)
    {
        Game.batch.setProjectionMatrix(camera.combined); //set the projection matrix to the HUD's camera
        Game.batch.begin();

        Game.batch.draw(texture, -camera.viewportWidth / 2, -camera.viewportHeight / 2, camera.viewportWidth, camera.viewportHeight);
        Game.batch.end();
    }
}