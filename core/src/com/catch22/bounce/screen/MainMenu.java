package com.catch22.bounce.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.catch22.bounce.audio.MusicPlayer;
import com.catch22.bounce.game.Bounce;
import com.catch22.bounce.gui.Button;
import com.catch22.bounce.gui.Event;

public class MainMenu implements Screen
{
    private static Stage stage = new Stage();

    //renderers
    public static OrthographicCamera camera; //camera to follow the balls around the world
    private static float tileSize;
    private Viewport gamePort; //the orthographic camera's viewport
    private OrthogonalTiledMapRenderer renderer; //tile renderer

    private static float buttonHeight, buttonWidth;
    private static int buttonX = 0;

    private static SpriteBatch gameBatch;
    private static SpriteBatch batch;

    static Texture dayBackground = new Texture("maps/texture background.png"), nightModeBackground = new Texture("maps/night mode background.png"),
            title = new Texture("title.png");

    private static TextButton playButton, newGameButton, cheatButton, aboutButton, settingsButton,
            muteButton, nightModeButton;

    private static int numberOfButtons = 0, numberOfSettingsButtons = 0;

    private int screenWidth, screenHeight;

    public MainMenu()
    {
        Gdx.input.setInputProcessor(stage);

        batch = new SpriteBatch();
        gameBatch = new SpriteBatch();

        //renderer initialization
        renderer = new OrthogonalTiledMapRenderer(Bounce.levels[Game.activeLevel].map, 1 / Bounce.PPM);

        //camera initialization
        camera = new OrthographicCamera();
        gamePort = new FitViewport(Bounce.width / Bounce.PPM, Bounce.height / Bounce.PPM, camera);
        camera.position.set(0, 0, 0);

        tileSize = gamePort.getWorldHeight() / 8;

        int ballTileY = (int) (Game.player.body.getPosition().y / tileSize);
        camera.position.y = gamePort.getWorldHeight() / 2 + ((ballTileY) / 7) * 7 * gamePort.getWorldHeight() / 8; //camera only translates along y, when ball is on different levels
        camera.translate(new Vector2(80 / Bounce.PPM, 20 / Bounce.PPM));
        camera.zoom = 2f;

        screenWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();

        buttonHeight = 0.20f;
        buttonWidth = 0.20f;
        buttonX = screenWidth / 8;
        playButton = createButton("Play");
        newGameButton = createButton("New Game");
//        aboutButton = createButton("About");
//        settingsButton = createButton("Settings");
        cheatButton = createButton("Cheat");

        muteButton = createSettingsButton("mute");
        nightModeButton = createSettingsButton("lights");

        Button.addClickEvent(playButton, new Event()
        {
            @Override
            public void action()
            {
                int lastLevel = 0;

                try
                {
                    // lastLevel = Game.fileHandler.readBytes()[0];
                } catch (Throwable neverGotPastLevelOne)
                {

                }

                lastLevel--;

                if(lastLevel > 0)
                {
                    Game.activeLevel = lastLevel;
                    Game.player.warp = true;

                    Bounce.master.setScreen(Bounce.game);
                }
                else
                {
                    Game.activeLevel = 1;

                    // Game.save();

                    Game.player.reposition(Bounce.levels[1].initialPosition);
                    Bounce.master.setScreen(Bounce.game);
                }
            }
        });

        Button.addClickEvent(newGameButton, new Event()
        {
            @Override
            public void action()
            {
                //set the starting level
                Game.activeLevel = 1;

                // Game.save();

                Game.player.reposition(Bounce.levels[1].initialPosition);
                Bounce.master.setScreen(Bounce.game);
            }
        });

//        Button.addClickEvent(aboutButton, new Event()
//        {
//            @Override
//            public void action()
//            {
//                dispose();
//                Bounce.master.setScreen(null);
//            }
//        });
//
//        Button.addClickEvent(settingsButton, new Event()
//        {
//            @Override
//            public void action()
//            {
//                dispose();
//                Bounce.master.setScreen(null);
//            }
//        });

        Button.addClickEvent(cheatButton, new Event()
        {
            @Override
            public void action()
            {
                Gdx.input.getTextInput(new Input.TextInputListener()
                {
                    @Override
                    public void input(String text)
                    {
                        if("787898".equals(text))
                            Bounce.Settings.CheatCodes.$787898 = true;
                        else if("ibelieveicanfly".equals(text))
                            Bounce.Settings.CheatCodes.ibelieveicanfly = true;
                        else if("twofaced".equals(text))
                            Bounce.Settings.CheatCodes.twofaced = true;
                        else if("antigravity".equals(text))
                            Bounce.Settings.CheatCodes.antigravity = true;
                        else if("vvvip".equals(text))
                            Bounce.Settings.CheatCodes.vvvip = true;
                    }

                    @Override
                    public void canceled()
                    {

                    }
                }, "Enter Cheat", "", "");
            }
        });

        Button.addClickEvent(muteButton, new Event()
        {
            @Override
            public void action()
            {
                muteButton.setText((Bounce.Settings.sound ? "un" : "") + "mute");
                Bounce.Settings.sound = !Bounce.Settings.sound;
                MusicPlayer.killAllSound();
                if (Bounce.Settings.sound)
                    MusicPlayer.play(Bounce.Settings.nightMode ? Bounce.backgroundMusicNight : Bounce.backgroundMusic);
            }
        });

        Button.addClickEvent(nightModeButton, new Event()
        {
            @Override
            public void action()
            {
                Bounce.Settings.nightMode = !Bounce.Settings.nightMode;
            }
        });
    }

    @Override
    public void show()
    {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta)
    {
        update();

        //clear the screen
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        gameBatch.setProjectionMatrix(camera.combined);

        batch.begin();
        batch.draw(Bounce.Settings.nightMode ? nightModeBackground : dayBackground, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        batch.end();

        //draw the player
        gameBatch.begin();
        Game.player.draw(gameBatch);
        gameBatch.end();

        renderer.render(new int[]{2}); //draw the map

        if(Bounce.Settings.nightMode)
        {
            Bounce.levels[Game.activeLevel].rayHandler.setCombinedMatrix(camera.combined);
            Bounce.levels[Game.activeLevel].rayHandler.render();
        }

        batch.begin();
        batch.draw(title, 0, screenHeight - (0.30f * screenHeight), screenWidth, 0.30f * screenHeight);
        batch.end();

        stage.draw();
    }

    public void handleInput()
    {
        if(Bounce.Settings.platform == "Desktop")
        {
            if(Gdx.input.isKeyPressed(Input.Keys.LEFT))
            {
                if(Game.player.body.getLinearVelocity().x >= -Game.player.maxVelocity)
                    Game.player.body.applyLinearImpulse(new Vector2(-Game.player.sidewaysImpulse, 0), Game.player.body.getWorldCenter(), true);
            }
            if(Gdx.input.isKeyPressed(Input.Keys.RIGHT))
            {
                if(Game.player.body.getLinearVelocity().x <= Game.player.maxVelocity)
                    Game.player.body.applyLinearImpulse(new Vector2(Game.player.sidewaysImpulse, 0), Game.player.body.getWorldCenter(), true);
            }
            if(Gdx.input.isKeyJustPressed(Input.Keys.SPACE))
                Game.player.jump();
        }

        //input for android device
        //this works, don't touch it; or do because it's android
        if(Gdx.input.getAccelerometerY() < 0 || Gdx.input.isKeyPressed(Input.Keys.LEFT))
            Game.player.move(false, Gdx.input.getAccelerometerY());
        else if(Gdx.input.getAccelerometerY() > 2 || Gdx.input.isKeyPressed(Input.Keys.RIGHT))
            Game.player.move(true, Gdx.input.getAccelerometerY());
        if(Gdx.input.justTouched())
            Game.player.jump();
    }

    private TextButton createButton(String text)
    {
        TextButton button = com.catch22.bounce.gui.TextButton.createButton(buttonX, Gdx.graphics.getHeight() / 2, buttonWidth, buttonHeight);
        button.setY(button.getY() - numberOfButtons * button.getHeight() / 2);
        stage.addActor(button);
        button.setName(text);
        button.setText(text);
        numberOfButtons++;
        return button;
    }

    private TextButton createSettingsButton(String text)
    {
        TextButton button = com.catch22.bounce.gui.TextButton.createButton(Gdx.graphics.getWidth(), 0, buttonHeight / 2, buttonHeight / 2);
        button.setX(button.getX() - (2 * numberOfSettingsButtons  + 1) * button.getWidth() / 2);
        button.setY(0);
        stage.addActor(button);
        button.setText(text);
        button.setName(text);
        numberOfSettingsButtons++;
        return button;
    }


    public void update()
    {
        handleInput();

        Bounce.levels[Game.activeLevel].world.step(Game.frameRate, Game.velocityIterations, Game.positionIterations); //step world

        renderer.setView(camera);
        camera.update();

        //update player
        Game.player.update();

        //update light
        if(Bounce.Settings.nightMode)
            Bounce.levels[Game.activeLevel].rayHandler.update();
    }

    @Override
    public void resize(int width, int height)
    {
        gamePort.update(width, height);
    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {

    }
}