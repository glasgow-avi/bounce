package com.catch22.bounce.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.catch22.bounce.audio.Audio;
import com.catch22.bounce.level.Level;
import com.catch22.bounce.screen.Game;

public class ResourceManager
{
    private static TmxMapLoader mapLoader = new TmxMapLoader();
    public static OrthogonalTiledMapRenderer renderer;
    private static OrthographicCamera camera;
    private static FitViewport gamePort;

    public static final String saveFileName = "save data/rnNJhsSx.dat";
    private static FileHandle fileHandler;

    public static void loadMaps()
    {
        mapLoader = new TmxMapLoader();

        for(int index = 0; index < Bounce.numberOfLevels; index++) //creates all levels and sets world contact listeners
        {
            Bounce.levels[index] = new Level(mapLoader.load("level" + index + ".tmx"));
            Bounce.levels[index].loadResources(index);
            Gdx.app.log("Loaded", "Level " + index);
        }
    }

    public static void loadRenderers()
    {
        renderer = new OrthogonalTiledMapRenderer(Bounce.levels[Game.activeLevel].map, 1  / Bounce.PPM);
        camera = new OrthographicCamera();
        gamePort = new FitViewport(Bounce.width / Bounce.PPM, Bounce.height / Bounce.PPM, camera);
        camera.position.set(gamePort.getWorldWidth() / 2, gamePort.getWorldHeight() / 2, 0);
        Game.tileSize = gamePort.getWorldHeight() / 8;
    }

    public static void loadFileHandle()
    {
        fileHandler = Gdx.files.local(saveFileName);
    }

    public static void loadAudio()
    {
        Audio.initializeAudio();

        Bounce.backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("audio/music/background.mp3"));
        Bounce.backgroundMusicNight = Gdx.audio.newMusic(Gdx.files.internal("audio/music/background night.mp3"));
    }

    public static void save()
    {
        try
        {
            fileHandler.writeBytes(new byte[]{(byte) Game.activeLevel}, false);
            //writes a file which contains the active level
        } catch(Throwable e)
        {
            Gdx.app.log("File", "Save failed");
        }
    }
}