/*
TODO:
draw more levels
implement game over
make better enemies
fix water - better water
music
 */

package com.catch22.bounce.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.physics.box2d.World;
import com.catch22.bounce.level.Level;
import com.catch22.bounce.screen.Game;
import com.catch22.bounce.screen.SplashScreen;

public class Bounce extends com.badlogic.gdx.Game
{
	public static float PPM = 100; //pixels per meter
    public static final int width = 186, height = 128; //width and height of the screen
	public static boolean backPressed = false;

	public static Bounce master;
	public static Screen menu, game;

	public static final int numberOfLevels = 8;
	public static Level[] levels = new Level[numberOfLevels]; //all the levels

	public static Music backgroundMusic, backgroundMusicNight;

	public static class Settings
	{
		public static boolean nightMode = false;
		public static boolean sound = true, music = true;

		public static final String platform = "Desktop"; //for controls

		public static class CheatCodes
		{
			public static boolean
				ibelieveicanfly,
				$787898,
				twofaced,
				antigravity,
				vvvip;
		}
	}

	public static int gravity = 10;

	public static void flipGravity(boolean yes)
	{
		World currentWorld = levels[Game.activeLevel].world;

		boolean antigravity = Settings.CheatCodes.antigravity;

		if(yes) //the ball is in water
			if(currentWorld.getGravity().y < 0 && !antigravity || currentWorld.getGravity().y > 0 && antigravity)
				currentWorld.setGravity(currentWorld.getGravity().scl(-0.1f));
		if(!yes)    //the ball is elsewhere
			if(!(currentWorld.getGravity().y < 0 && !antigravity || currentWorld.getGravity().y > 0 && antigravity))
				currentWorld.setGravity(Level.gravity);
	}

	@Override
	public void create()
	{
		master = this;
		SplashScreen splashScreen = new SplashScreen();
		setScreen(splashScreen);
	}

	@Override
	public void render()
	{
        super.render();
	}
}