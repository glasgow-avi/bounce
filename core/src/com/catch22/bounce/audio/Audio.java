package com.catch22.bounce.audio;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

import java.util.HashMap;

public class Audio
{
    public static final HashMap<String, Sound> clips = new HashMap<String, Sound>();

    public static Music bubbles;

    protected static final String[] fileNames = {"warp zone", "jump", "death", "life"};

    /**
     * Loads audio using list of file names in this class
     * Initializes HashMap from File Name to Sound Clip
     */
    public static final void initializeAudio()
    {
        bubbles = Gdx.audio.newMusic(Gdx.files.internal("audio/sound effects/bubbles.mp3"));

        for(String fileName : fileNames)
            clips.put(fileName, Gdx.audio.newSound(Gdx.files.internal("audio/sound effects/" + fileName + ".mp3")));
    }
}
