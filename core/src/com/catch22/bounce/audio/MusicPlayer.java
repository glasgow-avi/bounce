package com.catch22.bounce.audio;

import com.badlogic.gdx.audio.Music;
import com.catch22.bounce.game.Bounce;

public class MusicPlayer
{
    /**
     * Stops the sound effect that was playing
     */
    public static void killSound()
    {
        for(String fileName : Audio.fileNames)
        {
            try
            {
                Audio.clips.get(fileName).stop();
            }
            catch (Throwable soundWasNotEvenPlaying)
            {

            }
        }
    }

    /**
     * Stops the background music
     */
    public static void killMusic()
    {
        Bounce.backgroundMusic.stop();
        Bounce.backgroundMusicNight.stop();
    }

    public static void killAllSound()
    {
        killSound();
        killMusic();
    }

    public static void play(String fileName)
    {
        if(!Bounce.Settings.sound)
            return ;

        killSound();

        try
        {
            if(!Bounce.Settings.sound)
                throw new Throwable("audio is disabled");
            Audio.clips.get(fileName).play();
        }
        catch(Throwable uninitialized)
        {

        }
    }

    public static void play(Music music)
    {
        if(!Bounce.Settings.music)
            return ;

        killMusic();

        try
        {
            if(!Bounce.Settings.music)
                throw new Throwable("audio is disabled");
            music.play();
            music.setLooping(true);
        }
        catch(Throwable uninitialized)
        {

        }
    }
}