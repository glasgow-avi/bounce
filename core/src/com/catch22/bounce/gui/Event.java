package com.catch22.bounce.gui;

public interface Event
{
        void action();
}
