package com.catch22.bounce.gui;

import com.badlogic.gdx.Gdx;

public class ImageButton extends Button
{
    static
    {
        com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle style = new com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle();
        style.up = skin.getDrawable("buttonOff");
        style.down = skin.getDrawable("buttonOn");
    }

    public static com.badlogic.gdx.scenes.scene2d.ui.ImageButton createButton(int x, int y, float width, float height)
    {
        com.badlogic.gdx.scenes.scene2d.ui.ImageButton button = new com.badlogic.gdx.scenes.scene2d.ui.ImageButton((com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle) style);
        button.setWidth(width * Gdx.graphics.getWidth());
        button.setHeight(height * Gdx.graphics.getWidth());
        button.setPosition(x - button.getWidth() / 2, y - button.getHeight() / 2);

        return button;
    }
}
