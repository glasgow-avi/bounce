package com.catch22.bounce.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class TextButton extends Button
{
    static
    {
        style = new com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle();
        ((com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle)style).font = new BitmapFont();
        style.up = skin.getDrawable("buttonOff");
        style.down = skin.getDrawable("buttonOn");
    }

    public static com.badlogic.gdx.scenes.scene2d.ui.TextButton createButton(int x, int y, float width, float height)
    {
        com.badlogic.gdx.scenes.scene2d.ui.TextButton button = new com.badlogic.gdx.scenes.scene2d.ui.TextButton("", (com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle) style);
        button.setWidth(width * Gdx.graphics.getWidth());
        button.setHeight(height * Gdx.graphics.getWidth());
        button.setPosition(x - button.getWidth() / 2, y - button.getHeight() / 2);

        return button;
    }
}
