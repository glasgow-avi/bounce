package com.catch22.bounce.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class Button extends com.badlogic.gdx.scenes.scene2d.ui.Button
{
    protected static Skin skin;
    protected static ButtonStyle style;
    private static TextureAtlas textureAtlas;

    static
    {
        textureAtlas = new TextureAtlas("sprites/button/button.pack");
        skin = new Skin();
        skin.addRegions(textureAtlas);
    }

    public static void addClickEvent(final com.badlogic.gdx.scenes.scene2d.ui.Button b, final Event i)
    {
        b.addListener(new InputListener()
        {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                Gdx.app.log("Button", "Pressed " + b.getName());
                i.action();
                return super.touchDown(event, x, y, pointer, button);
            }
        });
    }
}