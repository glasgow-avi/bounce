package com.catch22.bounce.level;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.catch22.bounce.game.Bounce;
import com.catch22.bounce.sprites.Enemy;
import com.catch22.bounce.sprites.ExtraLife;
import com.catch22.bounce.sprites.Message;
import com.catch22.bounce.sprites.TileContactListener;
import com.catch22.bounce.sprites.WarpZone;

import box2dLight.RayHandler;

public class Level implements Disposable
{
    public RayHandler rayHandler;

    public World world; //each level is a separate world
    public TiledMap map; //the map associated with the level

    public static final TileContactListener contactListener = new TileContactListener(); //listens for contact between objects of the world

    public static Vector2 gravity = new Vector2(0, (Bounce.Settings.CheatCodes.antigravity ? 1 : -1) * Bounce.gravity); //gravity vector is set to negative by default, and positive if antigravity
    public Vector2 initialPosition; //the starting position of the ball

    public int numberOfCheckpoints, numberOfCheckpointsRemaining; //total number of checkpoints in the world, and number of checkpoints the user has not collected

    public Vector2[] waterCoordinates;
    /*
    logic: water coordinates consists of an array of pairs of coordinates
    even numbered indices denote the lower-left corner of a rectangle containing water
    odd numbered indices denote the upper-right corner of the rectangle with lower-left corner coordinates of preceding index
     */

    public WarpZone warpZone;
    public Array<Enemy> enemies = new Array<>();
    public Array<ExtraLife> extraLives = new Array<>();
    public Array<Message> messages = new Array<>();

    public Level(TiledMap map)
    {
        world = new World(gravity, true); //world constructor

        rayHandler = new RayHandler(world);
        rayHandler.setAmbientLight(.1f);

        this.map = map; //copy the map
   }

   public void loadResources(int levelNumber)
   {
       new LevelCreator(world, map); //get objects from map

       world.setContactListener(contactListener); //set the contact listener

       /**specific to level number
        * ball's initial position
        * number of checkpoints
        **/
       switch(levelNumber)
       {
           case 0:
               numberOfCheckpointsRemaining = -1;
               initialPosition = new Vector2(0, 0);
           case 1:
               numberOfCheckpointsRemaining = 8;
               initialPosition = new Vector2(32, 96);
               break;
           case 2:
               numberOfCheckpointsRemaining = 8;
               initialPosition = new Vector2(128, 96);
               break;
           case 3:
               numberOfCheckpointsRemaining = 10;
               initialPosition = new Vector2(768, 512);

               waterCoordinates = new Vector2[4];

               waterCoordinates[0] = new Vector2(12, 22);
               waterCoordinates[1] = new Vector2(21, 24);

               waterCoordinates[2] = new Vector2(75, 1);
               waterCoordinates[3] = new Vector2(111, 28);

               break;
           case 4:
               numberOfCheckpointsRemaining = 7;
               initialPosition = new Vector2(1560, 96);
               break;
           case 5:
               numberOfCheckpointsRemaining = 11;
               initialPosition = new Vector2(544, 388);

               waterCoordinates = new Vector2[10];

               waterCoordinates[0] = new Vector2(1, 1);
               waterCoordinates[1] = new Vector2(4, 3);

               waterCoordinates[2] = new Vector2(1, 4);
               waterCoordinates[3] = new Vector2(31, 29);

               waterCoordinates[4] = new Vector2(30, 6);
               waterCoordinates[5] = new Vector2(30, 29);

               waterCoordinates[6] = new Vector2(61, 1);
               waterCoordinates[7] = new Vector2(63, 7);

               waterCoordinates[8] = new Vector2(39, 8);
               waterCoordinates[9] = new Vector2(999, 999);

               break;
           case 6:
               numberOfCheckpointsRemaining = 10;
               initialPosition = new Vector2(898, 212);

               waterCoordinates = new Vector2[6];

               waterCoordinates[0] = new Vector2(44, 8);
               waterCoordinates[1] = new Vector2(51, 11);

               waterCoordinates[2] = new Vector2(60, 8);
               waterCoordinates[3] = new Vector2(66, 11);

               waterCoordinates[4] = new Vector2(66, 10);
               waterCoordinates[5] = new Vector2(68, 11);

               break;
           case 7:
               numberOfCheckpointsRemaining = 10;
               initialPosition = new Vector2(20, 430);

               waterCoordinates = new Vector2[2];

               waterCoordinates[0] = new Vector2(31, 22);
               waterCoordinates[1] = new Vector2(43, 25);

               break;

       }

       numberOfCheckpoints = numberOfCheckpointsRemaining; //the player has collected no checkpoints, at the start

       //scale down
       initialPosition.x /= Bounce.PPM;
       initialPosition.y /= Bounce.PPM;
   }

    @Override
    public void dispose()
    {
        rayHandler.dispose();
        map.dispose();
        world.dispose();
    }
}