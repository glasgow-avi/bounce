package com.catch22.bounce.level;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.EllipseMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Ellipse;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.catch22.bounce.game.Bounce;
import com.catch22.bounce.screen.Game;
import com.catch22.bounce.sprites.BeachBallCheckpoint;
import com.catch22.bounce.sprites.Checkpoint;
import com.catch22.bounce.sprites.Deflater;
import com.catch22.bounce.sprites.Enemy;
import com.catch22.bounce.sprites.ExtraLife;
import com.catch22.bounce.sprites.Inflater;
import com.catch22.bounce.sprites.Killer;
import com.catch22.bounce.sprites.Message;
import com.catch22.bounce.sprites.Nothing;
import com.catch22.bounce.sprites.Tile;
import com.catch22.bounce.sprites.WarpZone;

public class LevelCreator
{
    private static int level = 0;

    private void getObjects(World world, TiledMap map, int layer) throws IndexOutOfBoundsException
    {
        for(MapObject object : map.getLayers().get(layer).getObjects().getByType(RectangleMapObject.class))
        {
            Rectangle rect = ((RectangleMapObject)object).getRectangle();
            switch(layer)
            {
                case 3:
                    new Tile(world, map, rect);
                    break;
                case 4:
                    new Checkpoint(world, map, rect);
                    break;
                case 5:
                    new Killer(world, map, rect);
                    break;
                case 6:
                    Bounce.levels[level].warpZone = new WarpZone(world, map, rect, level);
                    break;
                case 7:
                    Bounce.levels[level].extraLives.add(new ExtraLife(world, map, rect));
                    break;
                case 9:
                    new Inflater(world, map, rect);
                    break;
                case 10:
                    new Deflater(world, map, rect);
                    break;
                case 11:
                    new BeachBallCheckpoint(world, map, rect);
                    break;
                case 12:
                    new Tile(world, map, rect).fixture.setRestitution(1.1f);
                    break;
                case 13:
                    Bounce.levels[level].messages.add(new Message(world, map, rect, object.getName()));
                    break;
                case 14:
                    new Nothing(world, map, rect);
                default:
            }
        }
    }

    final int enemyLayer = 8;
    private void getEnemies(World world, TiledMap map) throws IndexOutOfBoundsException
    {
        for(EllipseMapObject object : map.getLayers().get(enemyLayer).getObjects().getByType(EllipseMapObject.class))
        {
            Ellipse circle = object.getEllipse();
            boolean direction = Boolean.parseBoolean(object.getName());
            Bounce.levels[level].enemies.add(new Enemy(world, map, circle, direction));
        }
    }

    public LevelCreator(World world, TiledMap map)
    {
        Bounce.levels[level].extraLives = new Array<>();
        Bounce.levels[level].messages = new Array<>();
        for(int layer = 1; layer <= 14; layer++) //iterate through the layers, one by one, and get all the object of the layer
        {
            try
            {
                //rectangular objects
                getObjects(world, map, layer);
            } catch (IndexOutOfBoundsException nonExistentLayer)
            {

            }
        }

        Bounce.levels[level].enemies = new Array<>();
        Bounce.levels[level].enemies.clear();
        try
        {
            getEnemies(world, map);
        } catch (IndexOutOfBoundsException nonExistentLayer)
        {
            Gdx.app.log("Enemy", level + " Doesn't exist");
        }

        level++;
    }
}