package com.catch22.bounce.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Ellipse;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Shape2D;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.catch22.bounce.game.Bounce;

public abstract class Token
{
    protected World level;
    protected TiledMap map;
    protected Rectangle bounds;
    protected Ellipse enemy;
    protected Body body;
    protected FixtureDef fDef;
    public Fixture fixture;

    protected Texture texture;
    public boolean exists = true;

    public Token(World level, TiledMap map, Shape2D bounds, short bit)
    {
        this.level = level;
        this.map = map;

        BodyDef bDef = new BodyDef();
        fDef = new FixtureDef();

        if(bounds instanceof Rectangle)
        {
            PolygonShape shape = new PolygonShape();
            this.bounds = (Rectangle) bounds;
            bDef.type = BodyDef.BodyType.StaticBody;
            bDef.position.set((this.bounds.getX() + this.bounds.getWidth() / 2) / Bounce.PPM, (this.bounds.getY() + this.bounds.getHeight() / 2) / Bounce.PPM);
            shape.setAsBox(this.bounds.getWidth() / 2 / Bounce.PPM, this.bounds.getHeight() / 2 / Bounce.PPM);
            fDef.shape = shape;
        }
        else
        {
            CircleShape shape = new CircleShape();
            this.enemy = (Ellipse) bounds;
            bDef.type = BodyDef.BodyType.DynamicBody;
            bDef.position.set((this.enemy.x + this.enemy.height / 2) / Bounce.PPM, (this.enemy.y + this.enemy.height / 2) / Bounce.PPM);
            shape.setRadius(this.enemy.height / 2 / Bounce.PPM);
            fDef.shape = shape;
            fDef.density = 1000000;
        }

        body = level.createBody(bDef);

        fDef.restitution = 0.4F;

        fDef.filter.categoryBits = bit;

        fixture = body.createFixture(fDef);
    }

    public abstract void onHit();

    public void render(SpriteBatch batch)
    {
        if(!exists)
            return;

        batch.begin();
        float x = body.getPosition().x - bounds.getWidth() / 2 / Bounce.PPM, y = body.getPosition().y - bounds.getHeight() / 2 / Bounce.PPM;
        batch.draw(texture, x, y, bounds.getWidth() / Bounce.PPM, bounds.getHeight() / Bounce.PPM);
        batch.end();
    }
}