package com.catch22.bounce.sprites;

public class TileBits
{
    public static final short
    nothing = 1,
    tile = 2,
    checkpoint = 4,
    killer = 8,
    saveGame = 16,
    life = 32,
    enemy = 64,
    warpZone = 128,
    water = 256,
    lights = 512,
    superSpeed = 1024,
    sticky = 2048,
    inflater = 4096,
    ball = 8192,
    deflater = 16384,

    contactable = (short) (tile | checkpoint | killer | life | enemy | water | inflater | ball | deflater);
}
