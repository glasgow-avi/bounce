package com.catch22.bounce.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.World;
import com.catch22.bounce.game.Bounce;
import com.catch22.bounce.screen.Game;

public class Killer extends Token
{
    public Killer(World level, TiledMap map, Rectangle bounds)
    {
        super(level, map, bounds, TileBits.killer);
        fixture.setUserData(this);
    }

    @Override
    public void onHit()
    {
        if(Bounce.Settings.CheatCodes.$787898 == false)
            Game.player.dead = true;

        Gdx.app.log("Hit", "Killer");
    }
}
