package com.catch22.bounce.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Ellipse;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Disposable;
import com.catch22.bounce.game.Bounce;
import com.catch22.bounce.screen.Game;

public class Enemy extends Token implements Disposable
{
    public static final float speed = 65;
    public Vector2 velocity;

    private static final Texture enemy = new Texture("sprites/enemy.png");
    private static float width, height;
    private int currentState = 0;

    public Enemy(World level, TiledMap map, Ellipse circle, boolean direction)
    {
        super(level, map, circle, TileBits.enemy);

        width = super.enemy.width / Bounce.PPM;
        height = super.enemy.height / Bounce.PPM;

        fixture.setUserData(this);
        fDef.filter.maskBits = TileBits.tile | TileBits.checkpoint | TileBits.killer | TileBits.warpZone | TileBits.life | TileBits.enemy | TileBits.inflater | TileBits.deflater | TileBits.ball | TileBits.nothing;

        fDef.density = 1000000000;
        fDef.friction = 0;

        velocity = new Vector2(direction ? 0 : speed / Bounce.PPM, direction ? -speed / Bounce.PPM : 0);
        body.setLinearVelocity(velocity);
    }

    @Override
    public void dispose()
    {
        level.dispose();
        enemy.dispose();
    }

    @Override
    public void render(SpriteBatch batch)
    {
        float x = body.getPosition().x - super.enemy.width / Bounce.PPM / 2, y = body.getPosition().y - super.enemy.height / Bounce.PPM / 2;

        batch.begin();
        batch.draw(enemy, x, y, width / 2, height / 2, width, height, 1, 1, currentState * -36, 0, 0, 655, 655, false, false);
        currentState++;
        if(currentState == 10)
            currentState %= 10;

        batch.end();
    }

    public void update()
    {
        body.setLinearVelocity(velocity);
    }

    @Override
    public void onHit()
    {
        Game.player.dead = !Bounce.Settings.CheatCodes.$787898;

        Gdx.app.log("Hit", "Enemy");
    }

    public void reverse()
    {
        velocity.scl(-1);
        body.setLinearVelocity(velocity);
    }
}