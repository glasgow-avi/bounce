package com.catch22.bounce.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Disposable;
import com.catch22.bounce.audio.Audio;
import com.catch22.bounce.audio.MusicPlayer;
import com.catch22.bounce.game.Bounce;
import com.catch22.bounce.level.Level;
import com.catch22.bounce.screen.Game;
import com.catch22.bounce.screen.Hud;

import box2dLight.PointLight;

public class Ball extends Sprite implements Disposable
{
    public World level; //the level the ball is in
    public Body body; //the body of the ball

    private static PointLight light;

    public static Texture ball, beachball; //the texture of the ball

    public static float radius = 7; //radius
    public boolean avatar = true; //avatar is true if ball, false if beachball

    public boolean dead = false, warp = false;
    public static boolean deflate = false, inflate = false;

    public static Vector2 lastSavedPosition; //updated on every checkpoint collection

    private static final float bounceJump = 3.8f, beachballJump = 4.5f; //impulse for jumping (lower for glasgow, higher for beachball)
    public static float sidewaysImpulse = 0.1F, upwardImpulse = bounceJump; //impulse for moving sideways; set upward impulse to bounce jump by default
    public static float maxVelocity = 2; //the ball can't sideways go faster than here

    public static Thread ballDestroyer;

    private State previousState;
    public State currentState;

    protected enum State
    {
        FALLING, JUMPING, STANDING, RUNNING,
    }

    public State getState()
    {
        if(body.getLinearVelocity().y > 0 && currentState == State.JUMPING || body.getLinearVelocity().y < 0 && previousState == State.JUMPING)
            return State.JUMPING;
        else if(body.getLinearVelocity().y < 0)
            return State.FALLING;
        else if(body.getLinearVelocity().x != 0)
            return State.RUNNING;
        else
            return State.STANDING;
    }

    public Ball(Level level, float posX, float posY)
    {
        this.level = level.world;

        initializeBall(posX, posY);

        ball = new Texture("sprites/bounce.png");
        beachball = new Texture("sprites/beachball.png");

        setBounds(posX, posY, 2 * radius / Bounce.PPM, 2 * radius / Bounce.PPM);
        lastSavedPosition = new Vector2(getBoundingRectangle().x, getBoundingRectangle().y);
        setRegion(ball);
    }

    public void killBall()
    {
        body.applyLinearImpulse(new Vector2(5 / Bounce.PPM, 0), body.getWorldCenter(), false);

        if(Hud.numberOfLivesRemaining == 0)
        {
            Game.pause = true;
            Game.pauseScreen = !Bounce.Settings.nightMode ? Hud.gameOver : Hud.gameOverNight;
            Game.gameOver = true;
        }

        MusicPlayer.play("death");

        level.destroyBody(body);
        initializeBall(lastSavedPosition.x, lastSavedPosition.y);

        Hud.numberOfLivesRemaining--;
    }

    public void initializeBall(float x, float y)
    {
        BodyDef bDef = new BodyDef();
        bDef.position.set(x, y);
        bDef.type = BodyDef.BodyType.DynamicBody;
        //crate the body
        body = level.createBody(bDef);

        //create the fixture
        FixtureDef fDef = new FixtureDef();

        //set the fixture shape to a circle
        CircleShape circle = new CircleShape();
        circle.setRadius(radius / Bounce.PPM);
        fDef.shape = circle;
//        fDef.restitution = 0;

        //the ball can collide with whatever is listed
        fDef.filter.categoryBits = TileBits.ball;
        fDef.filter.maskBits = TileBits.tile | TileBits.checkpoint | TileBits.killer | TileBits.warpZone | TileBits.life | TileBits.enemy | TileBits.inflater | TileBits.deflater;

        //create the final player ball
        body.createFixture(fDef).setUserData("ball");

        Gdx.app.log("Ball", "Created ball in level " + Game.activeLevel);

        Bounce.levels[Game.activeLevel].rayHandler.removeAll();
        light = new PointLight(Bounce.levels[Game.activeLevel].rayHandler, 200, !avatar ? Color.YELLOW : Color.WHITE, 0.7f, 0, 0);
        light.setContactFilter(TileBits.lights, TileBits.contactable, TileBits.contactable);
        light.attachToBody(body);
    }

    public void reposition(Vector2 pos)
    {
        level.destroyBody(body);
        initializeBall(pos.x, pos.y);
    }

    public void dispose()
    {
        level.dispose();
        ball.dispose();
    }

    public void update()
    {
        previousState = getState();

        Bounce.flipGravity(!avatar && isInWater());

        if(inflate)
        {
            upwardImpulse = beachballJump; //change the ball's jumping ability
            Game.player.avatar = false; //change the player's avatar
            setTexture(beachball); //set the texture to the beachball texture
            if (Bounce.Settings.nightMode)
                light.setColor(Color.YELLOW);
            inflate = false; //don't have to inflate it in the next cycle
        }
        if(deflate)
        {
            upwardImpulse = bounceJump; //change the ball's jumping ability
            Game.player.avatar =  true; //change the player's avatar
            setTexture(ball); //set the texture to the ball texture
            if (Bounce.Settings.nightMode)
                light.setColor(Color.WHITE);
            deflate = false; //don't have to deflate it in the next cycle
        }
        if(warp)
            reposition(Bounce.levels[Game.activeLevel].initialPosition); //move the ball to the new world
        if(!dead)
            setPosition(body.getPosition().x - getWidth() / 2, body.getPosition().y - getHeight() / 2); //update the position of the ball
        else
        {
            if(ballDestroyer == null || !ballDestroyer.isAlive())
            {
                ballDestroyer = new BallDestroyer();
                ballDestroyer.start();
            }
//            killBall(); //murder the ball
            dead = false; //don't have to kill the ball in the next cycle}
        }

        currentState = getState();
    }

    public void move(boolean direction, float force)
    {
        if(direction ? body.getLinearVelocity().x <= maxVelocity : body.getLinearVelocity().x >= -maxVelocity)
            body.applyLinearImpulse(new Vector2((direction ? force / 5 : (force / 5)) * sidewaysImpulse, 0), body.getWorldCenter(), true);
    }


    public void jump()
    {
        if(currentState == State.JUMPING && !Bounce.Settings.CheatCodes.ibelieveicanfly)
            return;
        currentState = State.JUMPING;
//        if(Bounce.Settings.CheatCodes.ibelieveicanfly == true || body.getLinearVelocity().y == 0)
            body.applyLinearImpulse(new Vector2(0, (Bounce.Settings.CheatCodes.antigravity ? -1 : 1) * upwardImpulse), body.getWorldCenter(), true);

        MusicPlayer.play("jump");
    }

    private boolean isInWater()
    {
        Vector2[] waterCoordinates = Bounce.levels[Game.activeLevel].waterCoordinates;

        boolean isInWater = false;

        try
        {
            //check for each pair of coordinates, if the ball lies within that rectangle
            for(int coordinateIndex = 0; !isInWater && coordinateIndex < waterCoordinates.length; coordinateIndex += 2)
                 isInWater = Game.ballTileX >= waterCoordinates[coordinateIndex].x && Game.ballTileY >= waterCoordinates[coordinateIndex].y && Game.ballTileX <= waterCoordinates[coordinateIndex + 1].x && Game.ballTileY <= waterCoordinates[coordinateIndex + 1].y;
        }
        catch(NullPointerException theActiveLevelHasNoWater)
        {

        }

        if(!isInWater)
            Audio.bubbles.stop();
        else if(!Audio.bubbles.isPlaying())
            Audio.bubbles.play();

        return isInWater;
    }

    private static class BallDestroyer extends Thread
    {
        @Override
        public void run()
        {
//            for(int i = 1; i < 3; i++)
//            {
//                Game.player.setTexture(new Texture("sprites/dyingball" + i + ".png"));
//                try
//                {
//                    sleep(300);
//                } catch (InterruptedException e)
//                {
//                    e.printStackTrace();
//                }
//            }

            try
            {
                sleep(300);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }

            while (true)
            {
                try
                {
                    Game.player.killBall();
                    break;
                } catch (Throwable steppingWorld)
                {

                }
            }
        }
    }
}