package com.catch22.bounce.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

public class TileContactListener implements ContactListener
{
    @Override
    public void beginContact(Contact contact)
    {
        Fixture a = contact.getFixtureA(), b = contact.getFixtureB();

        if(a.getUserData() == "ball" || b.getUserData() == "ball")
        {
            Fixture ball = a.getUserData() == "ball" ? a : b, object = a.getUserData() == "ball" ? b : a;

            if(object.getUserData() != null && Token.class.isAssignableFrom(object.getUserData().getClass()))
                ((Token) object.getUserData()).onHit();
        }

        else if(a.getUserData() instanceof Enemy || b.getUserData() instanceof Enemy)
        {
            Fixture enemy = a.getUserData() instanceof Enemy ? a : b, object = a.getUserData() instanceof Enemy ? b : a;

//            if(enemy.getUserData() != null && Token.class.isAssignableFrom(enemy.getUserData().getClass()))
                ((Enemy) enemy.getUserData()).reverse();
        }
    }

    @Override
    public void endContact(Contact contact)
    {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold)
    {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse)
    {

    }
}