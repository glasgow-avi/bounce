package com.catch22.bounce.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.World;
import com.catch22.bounce.game.Bounce;
import com.catch22.bounce.screen.Game;
import com.catch22.bounce.screen.Hud;

public class Tile extends Token
{
    public Tile(World level, TiledMap map, Rectangle bounds)
    {
        super(level, map, bounds, TileBits.tile);
        fixture.setUserData(this);
    }

    public Tile(World level, TiledMap map, Polygon bounds)
    {
        super(level, map, bounds, TileBits.tile);
        fixture.setUserData(this);
    }

    @Override
    public void onHit()
    {
        if(Game.activeLevel == 1 && Game.theBeginning && Bounce.master.getScreen() instanceof Game)
        {
            Game.setPause(Hud.glasgowIntroduction);
            Game.theBeginning = false;
        }

        Gdx.app.log("Hit", "Tile");
    }
}
