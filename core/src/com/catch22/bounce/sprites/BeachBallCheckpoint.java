package com.catch22.bounce.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.GridPoint2;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.World;
import com.catch22.bounce.game.Bounce;
import com.catch22.bounce.level.Level;
import com.catch22.bounce.screen.Game;

public class BeachBallCheckpoint extends Token
{
    public BeachBallCheckpoint(World level, TiledMap map, Rectangle bounds)
    {
        super(level, map, bounds, TileBits.checkpoint);
        fixture.setUserData(this);
    }

    @Override
    public void onHit()
    {
        if(!Bounce.Settings.CheatCodes.twofaced && Game.player.avatar)
            return;
        Gdx.app.log("Hit", "Checkpoint");
        Filter filter = new Filter();
        filter.categoryBits = TileBits.nothing;
        fixture.setFilterData(filter);

        Gdx.app.log("Checkpoint:", String.valueOf(--Bounce.levels[Game.activeLevel].numberOfCheckpointsRemaining));
        Game.player.lastSavedPosition = new Vector2(Game.player.getBoundingRectangle().x, Game.player.getBoundingRectangle().y);
        Gdx.app.log("Saved:", Game.player.lastSavedPosition.x + " " + Game.player.lastSavedPosition.y);
    }
}
