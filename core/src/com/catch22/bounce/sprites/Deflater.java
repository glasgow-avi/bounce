package com.catch22.bounce.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.GridPoint2;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.World;
import com.catch22.bounce.level.Level;
import com.catch22.bounce.screen.Game;

public class Deflater extends Token
{
    public Deflater(World level, TiledMap map, Rectangle bounds)
    {
        super(level, map, bounds, TileBits.deflater);
        fixture.setUserData(this);
    }

    @Override
    public void onHit()
    {
        Gdx.app.log("Hit", "Deflater");

        Ball.deflate = true;
    }
}