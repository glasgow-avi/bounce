package com.catch22.bounce.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.GridPoint2;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.World;
import com.catch22.bounce.game.Bounce;
import com.catch22.bounce.level.Level;
import com.catch22.bounce.screen.Game;
import com.catch22.bounce.screen.Hud;
import com.catch22.bounce.screen.MainMenu;
import com.catch22.bounce.screen.TutorialManager;

public class Checkpoint extends Token
{
    public Checkpoint(World level, TiledMap map, Rectangle bounds)
    {
        super(level, map, bounds, TileBits.checkpoint);
        fixture.setUserData(this);
    }

    @Override
    public void onHit()
    {
        if(Bounce.master.getScreen() instanceof MainMenu)
            return;

        if(!(Bounce.Settings.CheatCodes.twofaced || Game.player.avatar))
            return;

        int numberOfCheckpoints = Bounce.levels[Game.activeLevel].numberOfCheckpoints - Bounce.levels[Game.activeLevel].numberOfCheckpointsRemaining;
        if(Game.activeLevel == 1 && Bounce.levels[Game.activeLevel].numberOfCheckpointsRemaining == Bounce.levels[Game.activeLevel].numberOfCheckpoints)
            Game.setPause(Hud.firstCheckpoint);

        if(Game.activeLevel == 1 && numberOfCheckpoints == 2)
        {
            Game.tutorialManager = new TutorialManager(1 / Bounce.PPM, 0, 8);
            Game.tutorialManager.start();
        }

        if(Game.activeLevel == 1 && Bounce.levels[Game.activeLevel].numberOfCheckpointsRemaining == 1)
            Game.setPause(Hud.firstWarped);

        Gdx.app.log("Hit", "Checkpoint");
        Filter filter = new Filter();
        filter.categoryBits = TileBits.nothing;
        fixture.setFilterData(filter);

        Gdx.app.log("Checkpoint:", String.valueOf(--Bounce.levels[Game.activeLevel].numberOfCheckpointsRemaining));
        Game.player.lastSavedPosition = new Vector2(Game.player.getBoundingRectangle().x, Game.player.getBoundingRectangle().y);
        Gdx.app.log("Saved:", Game.player.lastSavedPosition.x + " " + Game.player.lastSavedPosition.y);
    }
}
