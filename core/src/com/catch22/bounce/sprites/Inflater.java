package com.catch22.bounce.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.GridPoint2;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.World;
import com.catch22.bounce.level.Level;
import com.catch22.bounce.screen.Game;
import com.catch22.bounce.screen.Hud;

public class Inflater extends Token
{
    public Inflater(World level, TiledMap map, Rectangle bounds)
    {
        super(level, map, bounds, TileBits.inflater);
        fixture.setUserData(this);
    }

    @Override
    public void onHit()
    {
        Gdx.app.log("Hit", "Inflater");

        Ball.inflate = true;

        if(Game.activeLevel == 2)
            Game.setPause(Hud.beachball);
    }
}