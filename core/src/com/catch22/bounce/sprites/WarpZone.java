package com.catch22.bounce.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.World;
import com.catch22.bounce.audio.MusicPlayer;
import com.catch22.bounce.game.Bounce;
import com.catch22.bounce.screen.Game;
import com.catch22.bounce.screen.Hud;

public class WarpZone extends Token
{
    public WarpZone(World world, TiledMap map, Rectangle rect, int level)
    {
        super(world, map, rect, TileBits.warpZone);

        texture = new Texture("maps/level" + (level == Bounce.numberOfLevels - 1 ? "1" : (level + 1)) + ".png");

        fixture.setUserData(this);
    }

    @Override
    public void onHit()
    {
        if(!(Bounce.Settings.CheatCodes.vvvip || Bounce.levels[Game.activeLevel].numberOfCheckpointsRemaining == 0))
            return;

        Gdx.app.log("Ball Warped:", Game.activeLevel + " " + Game.player.getX() + " " + Game.player.getY());
        Hud.numberOfLivesRemaining++;

        Game.player.warp = true;

        //the ball always starts as a bounce
        Ball.deflate = true;

        MusicPlayer.play("warp zone");
    }
}