package com.catch22.bounce.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.World;
import com.catch22.bounce.game.Bounce;

public class Message extends Token
{
    public Message(World level, TiledMap map, Rectangle bounds, String name)
    {
        super(level, map, bounds, TileBits.nothing);

        try
        {
            texture = new Texture("tutorials/" + name + (Bounce.Settings.nightMode ? " night mode" : "") + ".png");
        }
        catch (Exception fileNotFound)
        {
            texture = new Texture("tutorials/" + name + (Bounce.Settings.CheatCodes.$787898 ? " immortal" : " mortal") + (Bounce.Settings.nightMode ? " night mode" : "") + ".png");
        }

        fixture.setUserData(this);

//        new Destroyer().start();
    }

    @Override
    public void onHit()
    {

    }
}