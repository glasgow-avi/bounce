package com.catch22.bounce.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.World;
import com.catch22.bounce.screen.Game;
import com.catch22.bounce.screen.Hud;

public class Nothing extends Token
{

    public Nothing(World level, TiledMap map, Rectangle bounds)
    {
        super(level, map, bounds, TileBits.nothing);
        fixture.setUserData(this);
    }

    @Override
    public void onHit()
    {

    }
}
