package com.catch22.bounce.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.World;
import com.catch22.bounce.audio.MusicPlayer;
import com.catch22.bounce.game.Bounce;
import com.catch22.bounce.screen.Game;
import com.catch22.bounce.screen.Hud;

public class ExtraLife extends Token
{
    private class Destroyer extends Thread
    {
        @Override
        public void run()
        {
            while (true)
            {
                try
                {
                    Bounce.levels[Game.activeLevel].world.destroyBody(body);
                    break;
                } catch (Throwable steppingWorld)
                {

                }
            }
        }
    }

    public ExtraLife(World level, TiledMap map, Rectangle bounds)
    {
        super(level, map, bounds, TileBits.life);
        texture = new Texture("sprites/extra_life.png");

        fixture.setUserData(this);
    }

    @Override
    public void onHit()
    {
        Gdx.app.log("Hit", "Extra Life");
        Filter filter = new Filter();
        filter.categoryBits = TileBits.nothing;
        exists = false;
        fixture.setFilterData(filter);

        MusicPlayer.play("life");

        new Destroyer().start();

        Hud.numberOfLivesRemaining++;
    }
}